import { createRouter, createWebHistory } from 'vue-router'
import Home from '../views/Home.vue'
import Orders from '../views/orders/Orders';
import CreateOrder from '../views/orders/CreateOrder';
import Products from '../views/products/Products';
import EditProduct from '../views/products/EditProduct';


const routes = [
  {
    path: '/',
    name: 'home',
    component: Home
  },
  {
    path: '/products',
    name: 'products',
    component: Products
  },
  {
    path: '/products/:id',
    name: 'editProduct',
    component: EditProduct
  },
  {
    path: '/orders',
    name: 'orders',
    component: Orders
  },
  {
    path: '/orders/create',
    name: 'createOrder',
    component: CreateOrder
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    // component: () => import(/* webpackChunkName: "about" */ '../views/About.vue')
  },
  {
		path      : '/auth/login',
		component : require('../views/auth/Login').default,
		name      : 'login'
	},
]

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes
})

export default router

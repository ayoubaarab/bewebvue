import LoadingComponent from '@/components/LoadingComponent';

export default {
    install(app){
        app.component('LoadingComponent',LoadingComponent);
    },
};
import axios from 'axios';
import { getToken } from '../shard/helpers/auth';

export const instance  = axios.create(
    {
        // baseURL: process.env.api_url,
        baseURL: 'http://127.0.0.1:8001/',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${getToken()}`  
        },
    }
);

export default {
    install(app){
        app.config.globalProperties.$http = instance;
    }
};
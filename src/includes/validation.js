import {
    Form as VeeForm,
    Field as VeeField,
    defineRule,
    ErrorMessage,
} from 'vee-validate';
  import {
    email,
    required,
    min, max,
    alpha_spaces as alphaSpaces,
    min_value as minVal,
    max_value as maxVal,
} from '@vee-validate/rules';
import ValidationErrors from '@/shard/components/ValidationErrors';  
import Alert from '@/shard/components/Alert';

export default {
    install(app) {
        app.component('VeeForm', VeeForm);
        app.component('VeeField', VeeField);
        app.component('ErrorMessage', ErrorMessage);
        //this component to handle backend error!!
        app.component('v-errors',ValidationErrors);
        app.component('appAlert',Alert);
        defineRule('required', required);
        defineRule('alpha_spaces', alphaSpaces);
        defineRule('email', email);
        defineRule('max', max);
        defineRule('min', min);
        defineRule('min_value', minVal);
        defineRule('max_value', maxVal);
    },
};
  
import { createApp } from 'vue';
import App from './App.vue'
import './registerServiceWorker';
import router from './router';
import store from './store';
import 'bootstrap/dist/css/bootstrap.min.css';
import 'bootstrap';
import VeeValidatePlugin from '@/includes/validation';
import AxiosPlugin from '@/includes/axios';
import ComponentPlugin from '@/includes/component';
import BootstrapVue3 from 'bootstrap-vue-3'
import VueToast from 'vue-toast-notification';
import 'vue-toast-notification/dist/theme-sugar.css';   


const app =  createApp(App)

app.use(store);
app.use(router);
app.use(VeeValidatePlugin);
app.use(AxiosPlugin);
app.use(ComponentPlugin);
app.use(BootstrapVue3);
app.use(VueToast);

app.mount('#app');

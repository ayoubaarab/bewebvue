export const is404 = (e)=>{
	return isErrorWithResponseAndStatus(e) && e.response.status == 404;
}
export const is422 = (e)=>{
	return  isErrorWithResponseAndStatus(e) && e.response.status == 422;
}
const isErrorWithResponseAndStatus = (e)=>{
	return e.response && e.response.status;
}
export function isLoggedIn()
{
	return localStorage.getItem("isLoggedIn") == "true";
}
export function logIn(token)
{
    localStorage.setItem('token',token);
	return localStorage.setItem('isLoggedIn',true);
}
export function logOut()
{
    localStorage.removeItem('token')
	return localStorage.setItem('isLoggedIn',false);
}
export function getToken()
{
	return localStorage.getItem('token');
}